// Import bootstrap
import { Button, Card, Row, Col } from 'react-bootstrap';

// import react components
import { Fragment } from 'react';


export default function Reviews () {
	return (
		<Fragment>
			<h2 className='text-center padding-top-5'>Reviews</h2>
			<Row>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Img variant="top" src="https://picsum.photos/286/180" />
						<Card.Body>
							<Card.Title><h2>Dina Macuja</h2></Card.Title>
							<Card.Text>Absolutely amazing place to eat, we will be making a reservation for our next visit.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Img variant="top" src="https://picsum.photos/286/180" />
						<Card.Body>
							<Card.Title><h2>Benny Bilang</h2></Card.Title>
							<Card.Text>Absolutely amazing! The place is beautiful and staff are super friendly and the food is delicious. I love that you get a lot of food as well for the price.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col md={12} lg={4} className='padding-top-5'>
					<Card className='cardHighlight'>
						<Card.Img variant="top" src="https://picsum.photos/286/180" />
						<Card.Body>
							<Card.Title><h2>Mary Christmas Aguinaldo</h2></Card.Title>
							<Card.Text>Everything about this restaurant was great. The place was clean and smelled good. The staff and greeter was very nice. Our waiter was awesome, the staff also worked as a team in bringing and cleaning up our food. The food was fresh and awesome.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			<Button variant='primary' block>See More Reviews!</Button>
		</Fragment>
	)
} 